﻿// derivative.cpp: определяет точку входа для приложения.
#include "derivative.h"
#include <conio.h>
#include <string>
#include <vector>
#include <stack>
#include <regex>

enum math { XDEGRE, ADEGRE, E, LN, LOG, SIN, COS, TG, CTG, ASIN, ACOS, ATG, ACTG, CONST, VARIABLE, PLUS, MINUS, MULT, DIV, EDEGRE, PI, CDEGRE }; //XDEGRE ADECRE LOG CONST VARIABLE

struct token {
	std::string value;
	math type;
};

bool is_XDEGRE(const std::string& input) {
	const std::regex r(R"([a-df-zA-DF-Z]\^[\[]?\d+[\/]?\d{0,9}[\]]?)");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_CDEGRE(const std::string& input) {
	const std::regex r(R"([\[]?[\deE]+[\/.,]?\d{0,9}[\]]?\^[\[]?[\deE]+[\/.,]?[\deE]{0,9}[\]]?)");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_EDEGRE(const std::string& input) {
	const std::regex r(R"([eE]\^[\[]?[a-zA-Z][\/]?[a-zA-Z]{0,9}[\]]?)");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_ADEGRE(const std::string& input) {
	const std::regex r(R"([\[]?\d+[.\/,]?\d{0,101}?[\]]?\^[a-df-zA-DF-Z])");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_LOG(const std::string& input) {
	const std::regex r(R"(log\d+)");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_CONST(const std::string& input) {
	const std::regex r(R"(\d+[.,\/]?\d{0,101})");
	std::smatch m;
	return std::regex_match(input, m, r);
}

bool is_VARIABLE(const std::string& input) {
	const std::regex r(R"([a-zA-Z])");
	std::smatch m;
	return std::regex_match(input, m, r);
}

std::vector<token> lexer(const std::string tests) {

	std::vector<token> lexems;
	std::stack<char> brackets;
	brackets.push('0');

	for (register int i = 0; i < tests.size(); ) {

		std::string temp;

		if (tests[i] != ' ') {
			if (tests[i] == '(') {
				brackets.push(tests[i]);
				++i;
			}
			else if (tests[i] == ')' && brackets.top() == '(') {
				brackets.pop();
				++i;
			}
			else if (tests[i] == '+') {
				lexems.push_back({ std::to_string(tests[i]), PLUS });
				++i;
			}
			else if (tests[i] == '-') {
				lexems.push_back({ std::to_string(tests[i]), MINUS });
				++i;
			}
			else if (tests[i] == '*') {
				lexems.push_back({ std::to_string(tests[i]), MULT });
				++i;
			}
			else if (tests[i] == ':') {
				lexems.push_back({ std::to_string(tests[i]), DIV });
				++i;
			}

			while ((tests[i] != '(' && tests[i] != ')' && tests[i] != '+' &&
				tests[i] != '-' && tests[i] != ':' && tests[i] != '*') && (i < tests.size())) {									// ':' becouse '/' use in regex for (n/m) fractionals
				temp += tests[i];
				++i;
			}
			
			if (temp == "e" || temp == "E") {
				lexems.push_back({ temp, E });
			}
			if (temp == "pi") {
				lexems.push_back({ temp, PI });
			}
			else if (temp == "ln") {
				lexems.push_back({ temp, LN });
			}
			else if (temp == "log") {
				lexems.push_back({ temp, LOG });
			}
			else if (temp == "sin") {
				lexems.push_back({ temp, SIN });
			}
			else if (temp == "cos") {
				lexems.push_back({ temp, COS });
			}
			else if (temp == "tg") {
				lexems.push_back({ temp, TG });
			}
			else if (temp == "ctg") {
				lexems.push_back({ temp, CTG });
			}
			else if (temp == "arcsin") {
				lexems.push_back({ temp, ASIN });
			}
			else if (temp == "arccos") {
				lexems.push_back({ temp, ACOS });
			}
			else if (temp == "arctg") {
				lexems.push_back({ temp, ATG });
			}
			else if (temp == "arcctg") {
				lexems.push_back({ temp, ACTG });
			}
			else if (is_EDEGRE(temp)) {
				lexems.push_back({ temp, EDEGRE });
			}
			else if (is_ADEGRE(temp)) {
				lexems.push_back({ temp, ADEGRE });
			}
			else if (is_CONST(temp)) {
				lexems.push_back({ temp, CONST });
			}
			else if (is_LOG(temp)) {
				lexems.push_back({ temp, LOG });
			}
			else if (is_XDEGRE(temp)) {
				lexems.push_back({ temp, XDEGRE });
			}
			else if (is_VARIABLE(temp)) {
				lexems.push_back({ temp, VARIABLE });
			}
			else if (is_CDEGRE(temp)) {
				lexems.push_back({ temp, CDEGRE });
			}
			/*else {
				std::cerr << " Incorrect function in input ";
				lexems.clear();
				return lexems;
			}*/	
		}
	}
	if (brackets.size() == 1) {
		return lexems;
	}
	else {
		std::cerr << " Incorrect brackets ";
		lexems.clear();
		return lexems;
	}	
}
int main(){

	std::string tests("x");
	std::cin >> tests;
	std::vector<token> results = lexer(tests);

	for (register int i = 0; i < results.size(); ++i) {
		std::cout << results[i].type;
		std::cout << "\n";
		std::cout << results[i].value;
		std::cout << "\n\n";
	}

	_getch();
	return 0;
}
